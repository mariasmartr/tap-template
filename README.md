# {{cookiecutter.tap_name}} Singer Tap

To use this cookie cutter template:

```bash
pip3 install pipx
pipx ensurepath
pipx install cookiecutter
```

```bash
cookiecutter https://gitlab.com/meltano/tap-template
```

See the [dev guide](https://gitlab.com/meltano/tap-base/-/blob/feature/initial-base-classes/docs/dev_guide.md).
